$(document).ready(function () {
    $('#compress-form').submit(function(e) {
        e.preventDefault();
        $('.progressbar').show()
        var data = new FormData($(this).get(0))
        console.log(data)
        console.log(uniqueId)
        $.ajax({
            headers: {
                'uniqueId': uniqueId
            },
            url: "http://localhost:8080/file-compress",
            type: 'POST',
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                console.log(data)
                // $(this)[0].reset()
                $('compress-form').reset()
            },
            error: function(err) {
                console.log(err)
            }
        })
    })
});
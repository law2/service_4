from django.urls import path, include
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('all_files', all_files, name="all_files"),
    path('compress/<int:id>', compressed_file, name="compress_file")
]

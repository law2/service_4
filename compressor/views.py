from django.shortcuts import render
import zlib as zl
from django.shortcuts import redirect
import zipfile as zf
import io
import os
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.conf import settings
import string, random
# Create your views here.
from django.core.files.storage import FileSystemStorage
from .models import *
from django.http import HttpResponse
def index(request):
    uniqueId = randomId()
    return render(request, 'index.html', {'uniqueId': uniqueId})

# def handle_upload_file(request):

def all_files(request):
    files = File.objects.all()
    return render(request, 'all_file.html', {
        'files': files
    })

def randomId():
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(10))

def compressed_file(request, id):
    media = settings.MEDIA_ROOT
    uncompressed_file = File.objects.get(id=id)
    filenames = str(uncompressed_file.files).split('/')[1]
    filename = filenames.split('.')[0]
    zip_subdir = "media"
    zip_name = "{}.zip".format(filename)
    s = io.BytesIO()
    with zf.ZipFile(zip_name, 'w') as file_bro:
        file_bro.write(os.path.join(media, 'media/'+filenames))
    # z = zf.ZipFile(s, "w")
    
    # z.write( os.path.join(media, 'media/'+filenames))
    # z.close()
    s.seek(0)
    zipped_file = os.path.join(media, filenames+'.zip')
    in_memory = InMemoryUploadedFile(s,None, filename+'.zip', 'application/zip', s.getbuffer().nbytes, None)
    zip_file = File(name=filename+'.zip', files=in_memory)
    fs = FileSystemStorage()
    zip_file.save()
    # zipdir(zipped_file, media)
    # fs.save(zip_file.name, )
    return redirect('/all_files')
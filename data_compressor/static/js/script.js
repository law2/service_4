$(document).ready(function () {
    $('#compress-form').submit(function(e) {
        e.preventDefault();
        var data = new FormData($(this).get(0))
        console.log(data)
        $.ajax({
            url: "http://localhost:8080/file-compress",
            type: $(this).attr('method'),
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                console.log(data)
                $(this).get(0).reset()
                this.reset()
            },
            error: function(err) {
                console.log(err)
            }
        })
    })
});
var ws = new SockJS('ws://127.0.0.1:15674/ws')
var client = Stomp.over(ws);
client.connect('guest', 'guest', on_connect, on_error, 'localhost')

var on_connect = function() {
    console.log('connected');
    subscribe()
};
var on_error =  function() {
    console.log('error');
};


function addResult(link) {
    $('result').html = '<div class="card pt-2">' +
    "<h6> Here's the link to download </h6>" +
    '<a style="font-size: 12px; color: gray; margin-bottom:0.5em;" href="' + link +
    '.zip">' + link +
    '.zip </a>' +
    '</div>'
}
function subscribe() {
    callback = function (message) {
        if (message.body) {
            var msg = message.body.split(" ");
            increase();
            if (msg[0] == "done") {
                addResult(msg[1]);
            }
        }
    }
    var subcription = client.subscribe(
        "/exchange/1706039471",
        callback
    )
}

function increase() {
    const progressbar = document.getElementsByClassName("progress-bar")[0];
    const progress =
        parseInt(progressbar.getAttribute("aria-valuenow")) + 10;
    if (progress <= 100) {
        progressbar.setAttribute("aria-valuenow", progress);
        progressbar.setAttribute("style", "width:" + progress + "%;");
        progressbar.innerHTML = progress + "%";
        if (progress == 100) {
            document.getElementsByClassName("status")[0].innerHTML =
                "Compresed!";
        }
    }
}